/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-stepper-motor-28byj-48-uln2003/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  Based on Stepper Motor Control - one revolution by Tom Igoe
*/

#include <Stepper.h>

const int stepsPerRevolution = 2048;  // change this to fit the number of steps per revolution


// initialize the stepper librarwith ULN2003 Motor Driver Pins
//Stepper myStepper(stepsPerRevolution, 13, 12, 14, 27);


int step_deg(float deg) 
{

  //int steps = round((stepsPerRevolution * deg) / 360.0); 
  Serial.println(deg);
  //Serial.println(steps);
  Serial.println();
  //myStepper.step(steps);
}

void setup() {
  // set the speed at 5 rpm
  //myStepper.setSpeed(15);
  // initialize the serial port
  Serial.begin(115200);
   
}

void loop() {
  step_deg(45.0);
  delay(200);
  step_deg(-45.0);
  delay(500);
  Serial.println("----");
  /*
  // step one revolution in one direction:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(1000);

  // step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution);
  delay(1000);
  */
}