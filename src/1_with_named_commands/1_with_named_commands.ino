/*!
 * Na základě https://github.com/DFRobot/DFRobot_DF2301Q 
 * přidal Ivo názvy příkazů z https://wiki.dfrobot.com/SKU_SEN0539-EN_Gravity_Voice_Recognition_Module_I2C_UART
 */
#include "DFRobot_DF2301Q.h"

// --- global variables -------------
uint8_t command_id = 0;

//I2C communication
DFRobot_DF2301Q_I2C DF2301Q;


void setup_serial() {
  Serial.begin(115200);

  // Init the sensor
  while (!(DF2301Q.begin())) {
    Serial.println("Communication with device failed, please check connection");
    delay(3000);
  }
  Serial.println("Serial ok!");
}

void setup_voice_recognition(
  uint8_t volume = 4,        // voice volume (1~7)
  bool mute = false,         // mute mode false/true
  uint8_t wakeTime = 15,     // wake-up duration (0-255)
  uint8_t start_command = 1  // 0 for no command, 1- Retreat, 23 - Wake-up command and soo (@see print_command for ID and names)
) {
  DF2301Q.setVolume(volume);
  DF2301Q.setMuteMode(mute ? 1 : 0);
  DF2301Q.setWakeTime(wakeTime);
  DF2301Q.playByCMDID(start_command);
}


void print_command(uint8_t command_id) {
  Serial.print(command_id);
  Serial.print(F(" - "));
  switch ((uint8_t)command_id) {
      // Wake-up words	ID
    case 1: Serial.println(F("Wake-up words for learning")); break;
    case 2: Serial.println(F("Hello robot")); break;

    // Commands for learning
    case 5: Serial.println(F("The first custom command")); break;
    case 6: Serial.println(F("The second custom command")); break;
    case 7: Serial.println(F("The third custom command")); break;
    case 8: Serial.println(F("The fourth custom command")); break;
    case 9: Serial.println(F("The fifth custom command")); break;
    case 10: Serial.println(F("The sixth custom command")); break;
    case 11: Serial.println(F("The seventh custom command")); break;
    case 12: Serial.println(F("The eighth custom command")); break;
    case 13: Serial.println(F("The ninth custom command")); break;
    case 14: Serial.println(F("The tenth custom command")); break;
    case 15: Serial.println(F("The eleventh custom command")); break;
    case 16: Serial.println(F("The twelfth custom command")); break;
    case 17: Serial.println(F("The thirteenth custom command")); break;
    case 18: Serial.println(F("The fourteenth custom command")); break;
    case 19: Serial.println(F("The fifteenth custom command")); break;
    case 20: Serial.println(F("The sixteenth custom command")); break;
    case 21: Serial.println(F("The seventeenth custom command")); break;

    // Fixed Command Words	ID
    case 22: Serial.println(F("Go forward")); break;
    case 23: Serial.println(F("Retreat")); break;
    case 24: Serial.println(F("Park a car")); break;
    case 25: Serial.println(F("Turn left ninety degrees")); break;
    case 26: Serial.println(F("Turn left forty-five degrees")); break;
    case 27: Serial.println(F("Turn left thirty degrees")); break;
    case 29: Serial.println(F("Turn right forty-five degrees")); break;
    case 30: Serial.println(F("Turn right thirty degrees")); break;
    case 31: Serial.println(F("Shift down a gear")); break;
    case 32: Serial.println(F("Line tracking mode")); break;
    case 33: Serial.println(F("Light tracking mode")); break;
    case 34: Serial.println(F("Bluetooth mode")); break;
    case 35: Serial.println(F("Obstacle avoidance mode")); break;
    case 36: Serial.println(F("Face recognition")); break;
    case 37: Serial.println(F("Object tracking")); break;
    case 38: Serial.println(F("Object recognition")); break;
    case 39: Serial.println(F("Line tracking")); break;
    case 40: Serial.println(F("Color recognition")); break;
    case 41: Serial.println(F("Tag recognition")); break;
    case 42: Serial.println(F("Object sorting")); break;
    case 43: Serial.println(F("Qr code recognition")); break;
    case 44: Serial.println(F("General settings")); break;
    case 45: Serial.println(F("Clear screen")); break;
    case 46: Serial.println(F("Learn once")); break;
    case 47: Serial.println(F("Forget")); break;
    case 48: Serial.println(F("Load model")); break;
    case 49: Serial.println(F("Save model")); break;
    case 50: Serial.println(F("Take photos and save them")); break;
    case 51: Serial.println(F("Save and return")); break;
    case 52: Serial.println(F("Display number zero")); break;
    case 53: Serial.println(F("Display number one")); break;
    case 54: Serial.println(F("Display number two")); break;
    case 55: Serial.println(F("Display number three")); break;
    case 56: Serial.println(F("Display number four")); break;
    case 57: Serial.println(F("Display number five")); break;
    case 58: Serial.println(F("Display number six")); break;
    case 59: Serial.println(F("Display number seven")); break;
    case 60: Serial.println(F("Display number eight")); break;
    case 61: Serial.println(F("Display number nine")); break;
    case 62: Serial.println(F("Display smiley face")); break;
    case 63: Serial.println(F("Display crying face")); break;
    case 64: Serial.println(F("Display heart")); break;
    case 65: Serial.println(F("Turn off dot matrix")); break;
    case 66: Serial.println(F("Read current posture")); break;
    case 67: Serial.println(F("Read ambient light")); break;
    case 68: Serial.println(F("Read compass")); break;
    case 69: Serial.println(F("Read temperature")); break;
    case 70: Serial.println(F("Read acceleration")); break;
    case 71: Serial.println(F("Reading sound intensity")); break;
    case 72: Serial.println(F("Calibrate electronic gyroscope")); break;
    case 73: Serial.println(F("Turn on the camera")); break;
    case 74: Serial.println(F("Turn off the camera")); break;
    case 75: Serial.println(F("Turn on the fan")); break;
    case 76: Serial.println(F("Turn off the fan")); break;
    case 77: Serial.println(F("Turn fan speed to gear one")); break;
    case 78: Serial.println(F("Turn fan speed to gear two")); break;
    case 79: Serial.println(F("Turn fan speed to gear three")); break;
    case 80: Serial.println(F("Start oscillating")); break;
    case 81: Serial.println(F("Stop oscillating")); break;
    case 82: Serial.println(F("Reset")); break;
    case 83: Serial.println(F("Set servo to ten degrees")); break;
    case 84: Serial.println(F("Set servo to thirty degrees")); break;
    case 85: Serial.println(F("Set servo to forty-five degrees")); break;
    case 86: Serial.println(F("Set servo to sixty degrees")); break;
    case 87: Serial.println(F("Set servo to ninety degrees")); break;
    case 88: Serial.println(F("Turn on the buzzer")); break;
    case 89: Serial.println(F("Turn off the buzzer")); break;
    case 90: Serial.println(F("Turn on the speaker")); break;
    case 91: Serial.println(F("Turn off the speaker")); break;
    case 92: Serial.println(F("Play music")); break;
    case 93: Serial.println(F("Stop playing")); break;
    case 94: Serial.println(F("The last track")); break;
    case 95: Serial.println(F("The next track")); break;
    case 96: Serial.println(F("Repeat this track")); break;
    case 97: Serial.println(F("Volume up")); break;
    case 98: Serial.println(F("Volume down")); break;
    case 99: Serial.println(F("Change volume to maximum")); break;
    case 100: Serial.println(F("Change volume to minimum")); break;
    case 101: Serial.println(F("Change volume to medium")); break;
    case 102: Serial.println(F("Play poem")); break;
    case 103: Serial.println(F("Turn on the light")); break;
    case 104: Serial.println(F("Turn off the light")); break;
    case 105: Serial.println(F("Brighten the light")); break;
    case 106: Serial.println(F("Dim the light")); break;
    case 107: Serial.println(F("Adjust brightness to maximum")); break;
    case 108: Serial.println(F("Adjust brightness to minimum")); break;
    case 109: Serial.println(F("Increase color temperature")); break;
    case 110: Serial.println(F("Decrease color temperature")); break;
    case 111: Serial.println(F("Adjust color temperature to maximum")); break;
    case 112: Serial.println(F("Adjust color temperature to minimum")); break;
    case 113: Serial.println(F("Daylight mode")); break;
    case 114: Serial.println(F("Moonlight mode")); break;
    case 115: Serial.println(F("Color mode")); break;
    case 116: Serial.println(F("Set to red")); break;
    case 117: Serial.println(F("Set to orange")); break;
    case 118: Serial.println(F("Set to yellow")); break;
    case 119: Serial.println(F("Set to green")); break;
    case 120: Serial.println(F("Set to cyan")); break;
    case 121: Serial.println(F("Set to blue")); break;
    case 122: Serial.println(F("Set to purple")); break;
    case 123: Serial.println(F("Set to white")); break;
    case 124: Serial.println(F("Turn on ac")); break;
    case 125: Serial.println(F("Turn off ac")); break;
    case 126: Serial.println(F("Increase temperature")); break;
    case 127: Serial.println(F("Decrease temperature")); break;
    case 128: Serial.println(F("Cool mode")); break;
    case 129: Serial.println(F("Heat mode")); break;
    case 130: Serial.println(F("Auto mode")); break;
    case 131: Serial.println(F("Dry mode")); break;
    case 132: Serial.println(F("Fan mode")); break;
    case 133: Serial.println(F("Enable blowing up & down")); break;
    case 134: Serial.println(F("Disable blowing up & down")); break;
    case 135: Serial.println(F("Enable blowing right & left")); break;
    case 136: Serial.println(F("Disable blowing right & left")); break;
    case 137: Serial.println(F("Open the window")); break;
    case 138: Serial.println(F("Close the window")); break;
    case 139: Serial.println(F("Open curtain")); break;
    case 140: Serial.println(F("Close curtain")); break;
    case 141: Serial.println(F("Open the door")); break;
    case 142: Serial.println(F("Close the door")); break;

    // Learning-related commands
    case 200: Serial.println(F("Learning wake word")); break;
    case 201: Serial.println(F("Learning command word")); break;
    case 202: Serial.println(F("Re-learn")); break;
    case 203: Serial.println(F("Exit learning")); break;
    case 204: Serial.println(F("I want to delete")); break;
    case 205: Serial.println(F("Delete wake word")); break;
    case 206: Serial.println(F("Delete command word")); break;
    case 207: Serial.println(F("Exit deleting")); break;
    case 208: Serial.println(F("Delete all")); break;

    default:
      Serial.print("Unknown command:");
      Serial.println(command_id);
      break;
  }
}

void action_for_command(uint8_t command_id) {
  print_command(command_id);
}

// classical Arduino functions
void setup() {
  setup_serial();
  setup_voice_recognition(7);  // defaulrt: volume=4, mute=false, wakeTime=15, start_command=1
}

void loop() {
  command_id = DF2301Q.getCMDID();
  if (0 != command_id) {
    action_for_command(command_id);
  }
  delay(200);
}
