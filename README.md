 
Experiments with Gravity: Offline Language Learning Voice Recognition Sensor
===========================================================================
for Arduino / Raspberry Pi / Python / ESP32 - I2C & UART

Links
-----
- [Wiki](https://wiki.dfrobot.com/SKU_SEN0539-EN_Gravity_Voice_Recognition_Module_I2C_UART)
- [GitHub](https://github.com/DFRobot/DFRobot_DF2301Q)
- [Simple video tutorial](https://www.youtube.com/watch?v=XlILfn9Z7H8) 
